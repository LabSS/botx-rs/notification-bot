CREATE TABLE IF NOT EXISTS "notification" (
    "id" uuid PRIMARY KEY NOT NULL,
    "send_at" timestamp with time zone NOT NULL,
    "text" text NOT NULL,
    "chat_id" uuid NOT NULL,
    "processed" boolean NOT NULL
);

CREATE TABLE IF NOT EXISTS "user_state" (
    "user_huid" uuid PRIMARY KEY NOT NULL,
    "last_message_id" uuid NOT NULL,
    "text_entering" boolean NOT NULL,
    "text" text NULL,
    "send_at" timestamp with time zone NULL,
    "pagination" int NULL
);