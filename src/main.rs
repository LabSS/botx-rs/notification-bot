pub mod handlers;
mod extensions;
pub mod models;
pub mod data;
pub mod utils;

use std::{net::Ipv4Addr, env, sync::Arc};

use actix_web::{HttpServer, App, middleware::Logger, web::{self, Data}};
use anthill_di::DependencyContext;
use async_lock::RwLock;
use botx_api_framework::botx_api::{extensions::botx_api_context::IBotXApiContextExt, api::context::BotXApiContext};
use botx_api_framework::extensions::{actix::IActixHandlersExt, anthill_di::IAnthillDiExt};
use env_logger::Env;
use sqlx::*;
use tokio::sync::oneshot;

use crate::{
    handlers::{
        message::MessageHandler,
        chat_created::ChatCreatedHandler,
        button::{
            create_timer::CreateTimerButtonHandler,
            confirm_new_timer::ConfirmNewTimerButtonHandler,
            reject_new_timer::RejectNewTimerButtonHandler,
            delete_timer::DeleteTimerButtonHandler,
            timers_list::TimersListButtonHandler,
            back_button::BackButtonHandler
        }
    },
    extensions::IAnthillDiDbExt, utils::notification_sender,
};

#[tokio::main]
async fn main() -> anyhow::Result<()> {
    dotenv::dotenv().ok();
    env_logger::Builder::from_env(Env::default().default_filter_or("info")).init();

    let (ip, port) = get_address();

    log::info!("Start server: {ip}:{port}");

    let connection_string = std::env::var("DATABASE_URL").expect("Unable to read DATABASE_URL env var");
    log::info!("Db connection string: {connection_string}");
    
    let ioc_ctx = DependencyContext::new_root();
    
    ioc_ctx.register_postgresql_connection_pool(connection_string).await?;

    ioc_ctx.register_botx_api_context().await?;
    ioc_ctx.register_botx_api_framework_context().await?;

    ioc_ctx.register_message_handler::<MessageHandler>().await?;
    ioc_ctx.register_chat_created_handler::<ChatCreatedHandler>().await?;
    ioc_ctx.register_button_handler::<CreateTimerButtonHandler>().await?;
    ioc_ctx.register_button_handler::<ConfirmNewTimerButtonHandler>().await?;
    ioc_ctx.register_button_handler::<RejectNewTimerButtonHandler>().await?;
    ioc_ctx.register_button_handler::<DeleteTimerButtonHandler>().await?;
    ioc_ctx.register_button_handler::<TimersListButtonHandler>().await?;
    ioc_ctx.register_button_handler::<BackButtonHandler>().await?;

    let db_connection_pool = ioc_ctx.resolve::<Pool<Postgres>>().await?;
    let api = ioc_ctx.resolve::<Arc<RwLock<BotXApiContext>>>().await?;

    let (stop_sender, stop_receiver) = oneshot::channel();
    let (completed_sender, completed_receiver) = oneshot::channel();

    tokio::spawn(async move {
        notification_sender(stop_receiver, completed_sender, db_connection_pool, api).await;
    });

    log::info!("Процесс нотификации запущен. Запускаем сервер...");

    HttpServer::new(move || {
        App::new()
            .wrap(Logger::default())
            .app_data(web::PayloadConfig::new(usize::MAX))
            .app_data(web::JsonConfig::default().limit(usize::MAX))
            .app_data(Data::new(ioc_ctx.clone()))
            .add_botx_api_handlers()
    })
    .bind((ip.to_string(), port))?
    .run()
    .await?;

    stop_sender.send(()).unwrap();
    completed_receiver.await.unwrap();

    Ok(())
}

const BOT_HOST_ENV: &str = "BOT_HOST";
const BOT_PORT_ENV: &str = "BOT_PORT";

fn get_address() -> (Ipv4Addr, u16) {
    let ip = env::var(BOT_HOST_ENV)
        .expect(&*format!("Env {BOT_HOST_ENV} not found"))
        .parse::<Ipv4Addr>()
        .expect(&*format!("Env {BOT_HOST_ENV} must be valid ipv4"));

    let port = env::var(BOT_PORT_ENV)
        .expect(&*format!("Env {BOT_PORT_ENV} not found"))
        .parse::<u16>()
        .expect(&*format!("Env {BOT_PORT_ENV} must be valid u32"));

    (ip, port)
}