use anthill_di::{DependencyContext, ServiceMappingBuilder, LifeCycle, types::AddDependencyResult};
use easy_ext::ext;
use sqlx::{postgres::PgPoolOptions, Pool, Postgres};

#[ext(IAnthillDiDbExt)]
#[async_trait_with_sync::async_trait]
pub impl DependencyContext {
    async fn register_postgresql_connection_pool(&self, connection_string: String) -> AddDependencyResult<ServiceMappingBuilder<Pool<Postgres>>> {
        let pool = PgPoolOptions::new()
            .max_connections(100)
            .connect(&connection_string)
            .await.expect("Не удалось установить соединение с базой данных");
        
        self.register_closure(move |_| Ok(pool.clone()), LifeCycle::Transient).await
    }
}

