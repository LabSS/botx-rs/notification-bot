use sqlx::types::{
    Uuid,
    chrono::*
};

#[derive(Debug)]
pub struct Notification {
    pub id: Uuid,
    pub send_at: DateTime<Utc>,
    pub text: String,
    pub chat_id: Uuid,
    pub processed: bool,
}

impl Notification {
    pub fn new(id: Uuid, send_at: DateTime<Utc>, text: String, chat_id: Uuid, processed: bool) -> Self {
        Self {
            id,
            send_at,
            text,
            chat_id,
            processed
        }
    }
}