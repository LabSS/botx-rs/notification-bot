use sqlx::types::{
    Uuid,
    chrono::*
};

#[derive(Debug)]
pub struct UserState {
    pub user_huid: Uuid,
    pub last_message_id: Uuid,
    pub text_entering: bool,
    pub text: Option<String>,
    pub send_at: Option<DateTime<Utc>>,
    pub pagination: Option<i32>,
}

impl UserState {
    pub fn new(user_huid: Uuid, last_message_id: Uuid, text_entering: bool, text: Option<String>, send_at: Option<DateTime<Utc>>, pagination: Option<i32>) -> Self {
        Self {
            user_huid,
            last_message_id,
            text_entering,
            text,
            send_at,
            pagination
        }
    }
}