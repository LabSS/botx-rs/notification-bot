use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{context::BotXApiContext, models::EventPayloadBuilder, v4::notification::direct::{models::DirectNotificationRequestBuilder, api::direct_notification}, utils::auth_retry::retry_with_auth, v3::events::edit_event::{api::edit_event, models::EditMessageRequestBuilder}},
    button_data,
    contexts::RequestContext,
    handlers::button::IButtonHandler,
    results::*
};
use sqlx::{Pool, Postgres, types::Uuid};

use crate::{handlers::message::{TimerButtonMetaData, base_message::send_base_message}, data, utils, models::notification::Notification};


#[button_data]
pub struct ConfirmNewTimerButtonData {}

#[derive(constructor)]
pub struct ConfirmNewTimerButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
    #[resolve]
    db_connection_pool: Pool<Postgres>,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for ConfirmNewTimerButtonHandler {
    type TData = ConfirmNewTimerButtonData;
    type TMetaData = TimerButtonMetaData;

    async fn handle(&mut self, _button_text: String, _data: Self::TData, _metadata: Self::TMetaData, request: RequestContext) -> CommandResult {
        log::info!("Сохранение таймера");
        
        utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, request.source_sync_id.unwrap(), "Таймер сохранен").await;

        let user_state = data::get_user_state(&self.db_connection_pool, request.from.user_huid.unwrap()).await;

        if let Some(mut user_state) = user_state {
            let response = send_base_message(&self.api, &request).await;

            let notification = Notification::new(Uuid::new_v4(), user_state.send_at.unwrap(), user_state.text.unwrap(), request.from.group_chat_id.unwrap(), false);

            let notification = data::insert_notification(&self.db_connection_pool, notification).await;

            user_state.text_entering = false;
            user_state.text = None;
            user_state.send_at = None;
            user_state.pagination = None;
            user_state.last_message_id = response.result.sync_id.unwrap();

            data::update_user_state(&self.db_connection_pool, &user_state).await;
        }

        Ok(CommandOk::default())
    }
}
