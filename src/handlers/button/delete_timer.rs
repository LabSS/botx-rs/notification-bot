use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    button_data,
    contexts::RequestContext,
    results::*,
    botx_api::api::context::BotXApiContext,
    handlers::button::IButtonHandler
};
use sqlx::{
    types::Uuid,
    Pool,
    Postgres
};

use crate::{handlers::message::{TimerButtonMetaData, base_message::send_base_message}, utils, data};

#[button_data]
pub struct DeleteTimerButtonData {
    pub notification_id: Uuid,
    pub name: String,
}

#[derive(constructor)]
pub struct DeleteTimerButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
    #[resolve]
    db_connection_pool: Pool<Postgres>,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for DeleteTimerButtonHandler {
    type TData = DeleteTimerButtonData;
    type TMetaData = TimerButtonMetaData;

    async fn handle(&mut self, _button_text: String, data: Self::TData, _metadata: Self::TMetaData, request: RequestContext) -> CommandResult {
        log::info!("Удаление таймера");

        utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, request.source_sync_id.unwrap(), &*format!("Удаляем таймер \"{}\"", data.name)).await;

        data::delete_notification(&self.db_connection_pool, data.notification_id).await;

        let response = send_base_message(&self.api, &request).await;

        let user_state = data::get_user_state(&self.db_connection_pool, request.from.user_huid.unwrap()).await;

        if let Some(mut user_state) = user_state {
            user_state.last_message_id = response.result.sync_id.unwrap();

            data::update_user_state(&self.db_connection_pool, &user_state).await;
        }

        Ok(CommandOk::default())
    }
}