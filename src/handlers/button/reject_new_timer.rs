use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{context::BotXApiContext, models::EventPayloadBuilder, v4::notification::direct::{models::DirectNotificationRequestBuilder, api::direct_notification}, utils::auth_retry::retry_with_auth, v3::events::edit_event::{api::edit_event, models::EditMessageRequestBuilder}},
    button_data,
    contexts::RequestContext,
    handlers::button::IButtonHandler,
    results::*
};
use sqlx::{Pool, Postgres};

use crate::{handlers::message::{base_message::send_base_message, TimerButtonMetaData}, data, utils};


#[button_data]
pub struct RejectNewTimerButtonData {}

#[derive(constructor)]
pub struct RejectNewTimerButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
    #[resolve]
    db_connection_pool: Pool<Postgres>,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for RejectNewTimerButtonHandler {
    type TData = RejectNewTimerButtonData;
    type TMetaData = TimerButtonMetaData;

    async fn handle(&mut self, _button_text: String, _data: Self::TData, _metadata: Self::TMetaData, request: RequestContext) -> CommandResult {
        log::info!("Отмена таймера");

        utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, request.source_sync_id.unwrap(), "Таймер отменен").await;

        let user_state = data::get_user_state(&self.db_connection_pool, request.from.user_huid.unwrap()).await;

        if let Some(mut user_state) = user_state {
            let response = send_base_message(&self.api, &request).await;

            user_state.text_entering = true;
            user_state.text = None;
            user_state.send_at = None;
            user_state.pagination = None;
            user_state.last_message_id = response.result.sync_id.unwrap();

            data::update_user_state(&self.db_connection_pool, &user_state).await;
        }

        Ok(CommandOk::default())
    }
}


