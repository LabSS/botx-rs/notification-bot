use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        context::BotXApiContext,
        models::*,
        v4::notification::direct::{
            models::DirectNotificationRequestBuilder,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        v3::events::edit_event::{
            api::edit_event,
            models::EditMessageRequestBuilder
        }
    },
    button_data,
    contexts::RequestContext,
    handlers::button::IButtonHandler,
    results::*
};
use sqlx::{Pool, Postgres};

use crate::{
    handlers::{
        message::TimerButtonMetaData,
        button::{
            delete_timer::DeleteTimerButtonData,
            back_button::BackButtonData
        }
    },
    data,
    utils
};

#[button_data]
pub struct TimersListButtonData {
    pub page: u16,
}

#[derive(constructor)]
pub struct TimersListButtonHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
    #[resolve]
    db_connection_pool: Pool<Postgres>,
}

#[async_trait_with_sync::async_trait]
impl IButtonHandler for TimersListButtonHandler {
    type TData = TimersListButtonData;
    type TMetaData = TimerButtonMetaData;

    async fn handle(&mut self, _button_text: String, _data: Self::TData, _metadata: Self::TMetaData, request: RequestContext) -> CommandResult {
        log::info!("Список таймеров таймера");

        utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, request.source_sync_id.unwrap(), "Составляем список таймеров").await;

        let notifications = data::get_not_processed_notification(&self.db_connection_pool, request.from.group_chat_id.unwrap()).await;

        let mut notifications = notifications.into_iter().map(|x| vec![BubbleBuilder::default()
            .with_label(format!("Удалить \"{}\"", x.text.get(0..20).map(|x| format!("{}...", x)).unwrap_or(x.text.clone())))
            .with_command("Удалить таймер")
            .with_data(DeleteTimerButtonData {
                type_id: Default::default(),
                notification_id: x.id,
                name: x.text.get(0..20).map(|x| format!("{}...", x)).unwrap_or(x.text.clone())
            })
            .build()
            .expect("Данные кнопки заполнены не верно")])
            .collect::<Vec<_>>();

        notifications.push(vec![BubbleBuilder::default()
            .with_label("Назад")
            .with_command("Назад")
            .with_data(BackButtonData { type_id: Default::default() })
            .build()
            .expect("Данные кнопки заполнены не верно")]);

        let notification = EventPayloadBuilder::default()
            .with_body("Список таймеров")
            .with_bubble(notifications)
            .with_metadata(TimerButtonMetaData { type_id: Default::default() })
            .build()
            .expect("Не все поля нотификации были указаны");

        let direct_notification_request = DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .expect("Не все поля запроса были указаны");

        let response = retry_with_auth(&self.api, || direct_notification(&self.api, &direct_notification_request)).await
            .expect("Не удалось отправить сообщение");

        let user_state = data::get_user_state(&self.db_connection_pool, request.from.user_huid.unwrap()).await;

        if let Some(mut user_state) = user_state {
            user_state.last_message_id = response.result.sync_id.unwrap();

            data::update_user_state(&self.db_connection_pool, &user_state).await;
        }

        Ok(CommandOk::default())
    }
}


