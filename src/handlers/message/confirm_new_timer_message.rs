use std::sync::Arc;

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::{EventPayloadBuilder, BubbleBuilder, BubbleOptionsBuilder, Align},
        v4::notification::direct::{
            models::*,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        context::BotXApiContext
    },
    contexts::RequestContext
};

use crate::handlers::{message::TimerButtonMetaData, button::{confirm_new_timer::ConfirmNewTimerButtonData, reject_new_timer::RejectNewTimerButtonData}};

pub async fn send_confirm_new_timer_message(api: &Arc<RwLock<BotXApiContext>>, request_context: &RequestContext) -> DirectNotificationResponse {
    let notification = EventPayloadBuilder::default()
        .with_body("Сохранить новый таймер?")
        .with_metadata(TimerButtonMetaData { type_id: Default::default() })
        .with_bubble(vec![
            vec![
                BubbleBuilder::default()
                    .with_label("Да")
                    .with_command("Да")
                    .with_data(ConfirmNewTimerButtonData { type_id: Default::default() })
                    .with_opts(BubbleOptionsBuilder::default().with_align(Align::Center).build().unwrap())
                    .build()
                    .expect("Данные кнопки заполнены не верно")
            ],
            vec![
                BubbleBuilder::default()
                    .with_label("Нет")
                    .with_command("Нет")
                    .with_data(RejectNewTimerButtonData { type_id: Default::default() })
                    .with_opts(BubbleOptionsBuilder::default().with_align(Align::Center).build().unwrap())
                    .build()
                    .expect("Данные кнопки заполнены не верно")
            ],
        ])
        .build()
        .expect("Не все поля нотификации были указаны");

    let direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request_context.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await
        .expect("Не удалось отправить сообщение")
}
