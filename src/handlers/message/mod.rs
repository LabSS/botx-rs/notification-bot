pub mod base_message;
pub mod time_enter_message;
pub mod error_time_format_message;
pub mod confirm_new_timer_message;

use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::context::BotXApiContext,
    handlers::message::IMessageHandler,
    contexts::RequestContext,
    results::*,
    button_data
};
use sqlx::{*, types::chrono::{DateTime, Utc, TimeZone}};

use crate::{
    models::user_state::UserState,
    data,
    handlers::message::{
        base_message::send_base_message,
        time_enter_message::send_time_enter_message,
        error_time_format_message::send_error_time_format_message,
        confirm_new_timer_message::send_confirm_new_timer_message
    },
    utils
};

#[button_data]
pub struct TimerButtonMetaData {}

#[derive(constructor)]
pub struct MessageHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
    #[resolve]
    db_connection_pool: Pool<Postgres>,
}

#[async_trait_with_sync::async_trait]
impl IMessageHandler for MessageHandler {
    async fn handle(&mut self, message: String, request_context: RequestContext) -> CommandResult {
        let user_state = data::get_user_state(&self.db_connection_pool, request_context.from.user_huid.unwrap()).await;

        if let Some(mut user_state) = user_state {
            match &user_state {
                UserState { text_entering: true, .. } => {
                    log::info!("Стадия ввода текста");
                    
                    utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, user_state.last_message_id, "Текст уведомления:").await;

                    let response = send_time_enter_message(&self.api, &request_context).await;

                    user_state.text = Some(message);
                    user_state.text_entering = false;
                    user_state.last_message_id = response.result.sync_id.unwrap();

                    data::update_user_state(&self.db_connection_pool, &user_state).await;
                }
                UserState { text_entering: false, text: Some(text), .. } => {
                    log::info!("Стадия ввода даты");

                    match Utc.datetime_from_str(&*message, "%Y-%m-%d %H:%M:%S") {
                        Ok(res) => {
                            utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, user_state.last_message_id, "Мы напомним в это время:").await;

                            let response = send_confirm_new_timer_message(&self.api, &request_context).await;

                            user_state.send_at = Some(res);
                            user_state.last_message_id = response.result.sync_id.unwrap();

                            data::update_user_state(&self.db_connection_pool, &user_state).await;
                        },
                        Err(err) => {
                            log::error!("{:#?}", err);

                            utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, user_state.last_message_id, "Дата в неверном формате:").await;
                            let response = send_error_time_format_message(&self.api, &request_context).await;
                            
                            user_state.last_message_id = response.result.sync_id.unwrap();

                            data::update_user_state(&self.db_connection_pool, &user_state).await;
                        },
                    }
                }
                _ => {
                    log::info!("Не найдена подходящая стадия,отправляет сообщение по умолчанию");

                    utils::update_message(TimerButtonMetaData { type_id: Default::default() }, &self.api, user_state.last_message_id, "Я помогу вам не забыть о важном!").await;

                    let direct_notification_response = send_base_message(&self.api, &request_context).await;
                    user_state.last_message_id = direct_notification_response.result.sync_id.unwrap();
                    data::update_user_state(&self.db_connection_pool, &user_state).await;
                }
            }
            
            return Ok(CommandOk::default());
        } else {
            let response = send_base_message(&self.api, &request_context).await;

            let user_state = UserState::new(request_context.from.user_huid.unwrap(), response.result.sync_id.unwrap(), false, None, None, None);

            let user_state = data::insert_user_state(&self.db_connection_pool, user_state).await;
        }

        Ok(CommandOk::default())
    }
}