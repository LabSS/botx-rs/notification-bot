use std::sync::Arc;

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::{
            BubbleBuilder,
            BubbleOptionsBuilder,
            Align, EventPayloadBuilder
        },
        v4::notification::direct::{
            models::*,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        context::BotXApiContext
    },
    contexts::RequestContext
};

use crate::handlers::{
    button::{
        create_timer::CreateTimerButtonData,
        delete_timer::DeleteTimerButtonData,
        timers_list::TimersListButtonData
    },
    message::TimerButtonMetaData
};


pub async fn send_base_message(api: &Arc<RwLock<BotXApiContext>>, request_context: &RequestContext) -> DirectNotificationResponse {
    let notification = EventPayloadBuilder::default()
        .with_body("Я помогу вам не забыть о важном!\nЧто вы хотите сделать?")
        .with_bubble(vec![
            vec![
                BubbleBuilder::default()
                    .with_label("Создай новый таймер")
                    .with_command("Создай новый таймер")
                    .with_data(CreateTimerButtonData { type_id: Default::default() })
                    .with_opts(BubbleOptionsBuilder::default().with_align(Align::Center).build().unwrap())
                    .build()
                    .expect("Данные кнопки заполнены не верно")
            ],
            vec![
                BubbleBuilder::default()
                    .with_label("Список таймеров")
                    .with_command("Список таймеров")
                    .with_data(TimersListButtonData { type_id: Default::default(), page: 0 })
                    .with_opts(BubbleOptionsBuilder::default().with_align(Align::Center).build().unwrap())
                    .build()
                    .expect("Данные кнопки заполнены не верно")
            ],
        ])
        .with_metadata(TimerButtonMetaData { type_id: Default::default() })
        .build()
        .expect("Не все поля нотификации были указаны");

    let direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request_context.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await
        .expect("Не удалось отправить сообщение")
}