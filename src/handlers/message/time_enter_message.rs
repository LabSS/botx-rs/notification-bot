use std::sync::Arc;

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::EventPayloadBuilder,
        v4::notification::direct::{
            models::*,
            api::direct_notification
        },
        utils::auth_retry::retry_with_auth,
        context::BotXApiContext
    },
    contexts::RequestContext
};

use crate::handlers::message::TimerButtonMetaData;


pub async fn send_time_enter_message(api: &Arc<RwLock<BotXApiContext>>, request_context: &RequestContext) -> DirectNotificationResponse {
    let notification = EventPayloadBuilder::default()
        .with_body("Введите дату и время в формате YY-mm-dd HH:MM:SS")
        .with_metadata(TimerButtonMetaData { type_id: Default::default() })
        .build()
        .expect("Не все поля нотификации были указаны");

    let direct_notification_request = DirectNotificationRequestBuilder::default()
        .with_group_chat_id(request_context.from.group_chat_id.unwrap())
        .with_notification(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    retry_with_auth(api, || direct_notification(api, &direct_notification_request)).await
        .expect("Не удалось отправить сообщение")
}