use std::sync::Arc;

use anthill_di_derive::constructor;
use async_lock::RwLock;
use botx_api_framework::botx_api::{bot::models::ChatCreatedCommand, api::{utils::auth_retry::retry_with_auth, v4::notification::direct::{api::direct_notification, models::DirectNotificationRequestBuilder}, context::BotXApiContext, models::EventPayloadBuilder}};
use botx_api_framework::{contexts::RequestContext, handlers::chat_created::IChatCreatedHandler, results::*};

#[derive(constructor)]
pub struct ChatCreatedHandler {
    #[resolve]
    api: Arc<RwLock<BotXApiContext>>,
}

#[async_trait_with_sync::async_trait]
impl IChatCreatedHandler for ChatCreatedHandler {
    async fn handle(&mut self, _event: ChatCreatedCommand<serde_json::Value>, request_context: RequestContext) -> CommandResult {
        let notification = EventPayloadBuilder::default()
            .with_body("Я добавлен в чат!\nЧтобы настроить напоминание отправьте сообщение упомянув меня")
            .build()
            .expect("Не все поля нотификации были указаны");

        let direct_notification_request = DirectNotificationRequestBuilder::default()
            .with_group_chat_id(request_context.from.group_chat_id.unwrap())
            .with_notification(notification)
            .build()
            .expect("Не все поля запроса были указаны");

        retry_with_auth(&self.api, || direct_notification(&self.api, &direct_notification_request)).await
            .expect("Не удалось отправить сообщение");

        Ok(CommandOk::default())
    }
}
