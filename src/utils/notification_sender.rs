use std::{sync::Arc, time::Duration};

use async_lock::RwLock;
use botx_api_framework::botx_api::api::{
    context::BotXApiContext,
    models::EventPayloadBuilder,
    v4::notification::direct::{
        models::DirectNotificationRequestBuilder,
        api::direct_notification
    },
    utils::auth_retry::retry_with_auth
};
use chrono::Utc;
use sqlx::{
    Pool,
    Postgres,
    types::Uuid
};
use tokio::{
    sync::oneshot::*,
    time::sleep
};

use crate::{handlers::message::TimerButtonMetaData, data};

pub async fn notification_sender(mut stop_receiver: Receiver<()>, completed_sender: Sender<()>, db_connection_pool: Pool<Postgres>, api: Arc<RwLock<BotXApiContext>>) {
    loop {
        let now = Utc::now() + chrono::Duration::hours(3);
        log::info!("Ищем нотификации до [{}]", now);
        let notifications = data::get_not_processed_notification_before_date(&db_connection_pool, now).await;
        log::info!("Нашли [{}] таймеров", notifications.len());

        let mut success_notifications = Vec::<Uuid>::new();
        for notification in notifications {
            let payload = EventPayloadBuilder::default()
                .with_body(notification.text)
                .with_metadata(TimerButtonMetaData { type_id: Default::default() })
                .build()
                .expect("Не все поля нотификации были указаны");

            let direct_notification_request = DirectNotificationRequestBuilder::default()
                .with_group_chat_id(notification.chat_id)
                .with_notification(payload)
                .build()
                .expect("Не все поля запроса были указаны");

            match retry_with_auth(&api, || direct_notification(&api, &direct_notification_request)).await {
                Ok(_) => {
                    log::info!("Обработали нотификацию [{}]", notification.id);
                    success_notifications.push(notification.id);
                },
                Err(err) => {
                    log::error!("Не удалось отправить нотификацию [{}], ошибка [{}]", notification.id, err);
                }
            }
        }

        if success_notifications.len() > 0 {
            log::info!("Помечаем [{}] как выполненные", success_notifications.len());
            data::mark_notification_as_processed(&db_connection_pool, success_notifications).await;
        } else {
            log::info!("Не нашлось нотификаций для обновления состояния");
        }

        sleep(Duration::from_secs(10)).await;

        if let Ok(_) = stop_receiver.try_recv() {
            log::info!("Получено уведомление, завершаем рассылку");
            break;
        }
    }

    completed_sender.send(()).unwrap();
}