use std::sync::Arc;

use async_lock::RwLock;
use botx_api_framework::{
    botx_api::api::{
        models::EventPayloadBuilder,
        v3::events::edit_event::{
            models::EditMessageRequestBuilder,
            api::edit_event
        },
        context::BotXApiContext,
        utils::auth_retry::retry_with_auth
    },
};
use sqlx::types::Uuid;



pub async fn update_message<TMetaData: Into<serde_json::Value>>(metadata: TMetaData, api: &Arc<RwLock<BotXApiContext>>, message_id: Uuid, text: &str) {
    let notification = EventPayloadBuilder::default()
        .with_body(text)
        .with_bubble(vec![vec![]])
        .with_metadata(metadata)
        .build()
        .expect("Не все поля нотификации были указаны");

    let edit_event_request = EditMessageRequestBuilder::default()
        .with_sync_id(message_id)
        .with_payload(notification)
        .build()
        .expect("Не все поля запроса были указаны");

    let _edit_event_request_response = retry_with_auth(api, || edit_event(api, &edit_event_request)).await
        .expect("Не удалось отправить сообщение");
}