use sqlx::{Pool, Postgres, types::{Uuid, chrono::{DateTime, Utc}}};

use crate::models::notification::Notification;

pub async fn insert_notification(db_connection_pool: &Pool<Postgres>, notification: Notification) -> Notification {
    sqlx::query_as!(
        Notification,
        "INSERT INTO notification (id, send_at, \"text\", chat_id, processed) VALUES ($1, $2, $3, $4, $5) RETURNING *",
        notification.id,
        notification.send_at,
        notification.text,
        notification.chat_id,
        notification.processed,
    )
    .fetch_one(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных")
}

pub async fn get_not_processed_notification_before_date(db_connection_pool: &Pool<Postgres>, search_date: DateTime<Utc>) -> Vec<Notification> {
    sqlx::query_as!(
        Notification,
        "select id, send_at, \"text\", chat_id, processed from notification where send_at < $1 AND processed = false",
        search_date,
    )
    .fetch_all(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных")
}

pub async fn mark_notification_as_processed(db_connection_pool: &Pool<Postgres>, notification_ids: Vec<Uuid>) {
    sqlx::query!(
        "UPDATE notification SET processed = true WHERE id = ANY($1)",
        notification_ids.as_slice(),
    )
    .execute(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных");
}

pub async fn get_not_processed_notification(db_connection_pool: &Pool<Postgres>, chat_id: Uuid) -> Vec<Notification> {
    sqlx::query_as!(
        Notification,
        "select id, send_at, \"text\", chat_id, processed from notification where processed = false AND chat_id = $1",
        chat_id
    )
    .fetch_all(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных")
}

pub async fn delete_notification(db_connection_pool: &Pool<Postgres>, id: Uuid) {
    sqlx::query!(
        "DELETE FROM notification WHERE id = $1",
        id,
    )
    .execute(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных");
}