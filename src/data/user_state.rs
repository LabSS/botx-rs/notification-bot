use sqlx::{Pool, Postgres, types::Uuid};

use crate::models::user_state::UserState;

pub async fn insert_user_state(db_connection_pool: &Pool<Postgres>, user_state: UserState) -> UserState {
    sqlx::query_as!(
        UserState,
        "INSERT INTO user_state (user_huid, last_message_id, text_entering, \"text\", send_at, pagination) VALUES ($1, $2, $3, $4, $5, $6) RETURNING *",
        user_state.user_huid,
        user_state.last_message_id,
        user_state.text_entering,
        user_state.text,
        user_state.send_at,
        user_state.pagination,
    )
    .fetch_one(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных")
}

pub async fn get_user_state(db_connection_pool: &Pool<Postgres>, user_huid: Uuid) -> Option<UserState> {
    sqlx::query_as!(
        UserState,
        "select user_huid, last_message_id, text_entering, \"text\", send_at, pagination from user_state where user_huid = $1",
        user_huid
    )
    .fetch_optional(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных")
}

pub async fn update_user_state(db_connection_pool: &Pool<Postgres>, user_state: &UserState) {
    sqlx::query!(
        "UPDATE user_state SET last_message_id = $2, text_entering = $3, \"text\" = $4, send_at = $5, pagination = $6 where user_huid = $1",
        user_state.user_huid,
        user_state.last_message_id,
        user_state.text_entering,
        user_state.text,
        user_state.send_at,
        user_state.pagination,
    )
    .execute(db_connection_pool)
    .await
    .expect("Ошибка запроса к базе данных");
} 