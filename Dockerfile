FROM rust:1.67

WORKDIR /usr/src/notification-bot
COPY . .

RUN rustup default nightly

RUN cargo install --path .

CMD ["notification-bot"]